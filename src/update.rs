use std::{error::Error, time::Instant};

use rstar::RTree;
use slog::{info, warn, Logger};
use spring_ai_rs::{
    ai_interface::{
        callback::{
            command::options::UnitCommandOptions, position::GroundPosition, unit_def::UnitDef,
        },
        AIInterface,
    },
    ai_macro::ai_func,
};

use crate::{
    project_selection::{simple_construction_project_selection, ProjectType},
    random_choice, random_number, AIType,
};

#[ai_func("update")]
fn update(logger: &Logger, ai_interface: AIInterface, frame: i32) -> Result<(), Box<dyn Error>> {
    if frame % 15 == 0 {
        let start = Instant::now();

        let ai_type = ai_interface.load_memory::<AIType>("AIType").unwrap();

        let mut current_energy = 0.0;
        for resource in ai_interface.resource_interface().get_resources()? {
            if resource.name()? == "Energy" {
                current_energy = resource.current()?;
            }
        }

        // TODO: CheaterAI Crashes
        // if ai_type == AIType::SimpleCheaterAI {
        //     for resource in ai_interface.resource_interface().get_resources()? {
        //         if resource.current()? < resource.storage()? * 0.2 {
        //             info!(
        //                 logger,
        //                 "Cheat: Giving {} {}",
        //                 resource.storage()? * 0.25,
        //                 resource.name()?
        //             );
        //             ai_interface
        //                 .cheat()
        //                 .give_resource(resource, resource.storage()? * 0.25)
        //                 .unwrap()
        //         }
        //     }
        // }

        let team_units = ai_interface.unit_interface().team_units()?;
        let friendly_units = ai_interface.unit_interface().friendly_units()?;
        let enemy_units = ai_interface.unit_interface().enemy_units()?;

        let enemy_tree = RTree::bulk_load(
            enemy_units
                .iter()
                .map(|enemy| {
                    let enemy_position = enemy.position().unwrap();
                    [enemy_position[0], enemy_position[2]]
                })
                .collect::<Vec<_>>(),
        );

        for &unit in &team_units {
            let unit_position = unit.position()?;

            let commander_defs = ai_interface
                .load_memory::<Vec<UnitDef>>("SimpleCommanderDefs")
                .unwrap();
            if commander_defs.contains(&unit.unit_def()?) {
                // if let Some(nearest_enemy_cloak) = unit.get_nearest_enemy(2000.0, false)? {
                //     info!(logger, "Commander near cloaked enemy");
                //     if nearest_enemy_cloak.is_cloaked()? && current_energy > 1000.0 {
                //         // Spring.GiveOrderToUnit(unitID, 37382, {1}, 0)
                //         unit.attack(&[], None, None, nearest_enemy_cloak)?;
                //     } else {
                //         // Spring.GiveOrderToUnit(unitID, 37382, {0}, 0)
                //         unit.attack(&[], None, None, nearest_enemy_cloak)?;
                //     }
                // }

                if let Some(nearest_enemy) = unit.get_nearest_enemy(250.0, false)? {
                    let unit_health_precentage = unit.health()? / unit.max_health()?;

                    if unit_health_precentage > 0.3 {
                        info!(logger, "Commander attacking nearby enemies");
                        unit.dgun_unit(&[], None, None, nearest_enemy)?;

                        let nearest_enemies = ai_interface.unit_interface().enemy_units_at(
                            unit.position()?,
                            300.0,
                            true,
                        )?;

                        for enemy in nearest_enemies {
                            if enemy.team()? == nearest_enemy.team()? && enemy != nearest_enemy {
                                unit.dgun_unit(&[UnitCommandOptions::ShiftKey], None, None, enemy)?;
                            }
                        }

                        unit.move_unit(
                            &[UnitCommandOptions::ShiftKey],
                            None,
                            Some(frame + 10000),
                            unit.position()?,
                        )?;
                    } else {
                        info!(logger, "Commander avoiding nearby enemies");
                        for _ in 0..10 {
                            let target_unit = random_choice(&team_units);
                            if !target_unit.unit_def()?.movement().able_to_move()? {
                                let target_position = target_unit.position()?;
                                unit.move_unit(
                                    &[],
                                    None,
                                    Some(frame + 10000),
                                    GroundPosition(
                                        target_position[0] + random_number(-100.0, 100.0),
                                        target_position[2] + random_number(-100.0, 100.0),
                                    ),
                                )?;
                                break;
                            }
                        }
                    }
                }
            }

            let constructor_defs = ai_interface
                .load_memory::<Vec<UnitDef>>("SimpleConstructorDefs")
                .unwrap();
            if constructor_defs.contains(&unit.unit_def()?) {
                let unit_health_precentage = unit.health()? / unit.max_health()?;
                if let Some(nearest_enemy) = unit.get_nearest_enemy(500.0, false)? {
                    let target_position = nearest_enemy.position()?;
                    if unit_health_precentage > 0.9 {
                        info!(logger, "Constructor reclaiming nearby enemy");
                        unit.fight(
                            &[],
                            None,
                            None,
                            GroundPosition(target_position[0], target_position[2]),
                        )?;
                    } else {
                        info!(logger, "Constructor avoiding nearby enemies");
                        for _ in 0..10 {
                            let target_unit = random_choice(&team_units);
                            if !target_unit.unit_def()?.movement().able_to_move()? {
                                let target_position = target_unit.position()?;
                                unit.move_unit(
                                    &[],
                                    None,
                                    Some(frame + 10000),
                                    GroundPosition(
                                        target_position[0] + random_number(-100.0, 100.0),
                                        target_position[2] + random_number(-100.0, 100.0),
                                    ),
                                )?;
                                break;
                            }
                        }
                    }
                }
            }

            let current_commands = unit.current_commands()?;

            let timeout = current_commands
                .first()
                .map(|c| frame > c.timeout().unwrap())
                .unwrap_or(false);

            if timeout {
                unit.move_unit(&[], None, Some(0), GroundPosition(0.0, 0.0))?;
            }

            if current_commands.is_empty() || timeout {
                let factory_defs = ai_interface
                    .load_memory::<Vec<UnitDef>>("SimpleFactoryDefs")
                    .unwrap();

                let undefined_unit_defs = ai_interface
                    .load_memory::<Vec<UnitDef>>("SimpleUndefinedUnitDefs")
                    .unwrap();

                if constructor_defs.contains(&unit.unit_def()?) {
                    simple_construction_project_selection(
                        logger,
                        ai_interface,
                        frame,
                        &team_units,
                        &friendly_units,
                        &enemy_units,
                        unit,
                        ProjectType::Constructor,
                    )?;
                }

                if commander_defs.contains(&unit.unit_def()?) {
                    simple_construction_project_selection(
                        logger,
                        ai_interface,
                        frame,
                        &team_units,
                        &friendly_units,
                        &enemy_units,
                        unit,
                        ProjectType::Commander,
                    )?;
                }

                if factory_defs.contains(&unit.unit_def()?) {
                    simple_construction_project_selection(
                        logger,
                        ai_interface,
                        frame,
                        &team_units,
                        &friendly_units,
                        &enemy_units,
                        unit,
                        ProjectType::Factory,
                    )?;
                }

                if undefined_unit_defs.contains(&unit.unit_def()?) {
                    if ai_type == AIType::SimpleDefenderAI {
                        info!(logger, "Defending with army");
                        let target_unit = random_choice(&team_units);
                        let target_position = target_unit.position()?;
                        unit.move_unit(
                            &[UnitCommandOptions::ShiftKey],
                            None,
                            Some(frame + 10000),
                            GroundPosition(
                                target_position[0] + random_number(-100.0, 100.0),
                                target_position[2] + random_number(-100.0, 100.0),
                            ),
                        )?;
                    } else {
                        if let Some(nearest_enemy_location) =
                            enemy_tree.nearest_neighbor(&[unit_position[0], unit_position[2]])
                        {
                            let distance = ((nearest_enemy_location[0] - unit_position[0]).powi(2)
                                + (nearest_enemy_location[1] - unit_position[2]).powi(2))
                            .sqrt();
                            if distance < 2000.0 {
                                info!(logger, "Army attacking nearby enemy");
                                unit.fight(
                                    &[UnitCommandOptions::ShiftKey],
                                    None,
                                    Some(frame + 10000),
                                    GroundPosition(
                                        nearest_enemy_location[0] + random_number(-100.0, 100.0),
                                        nearest_enemy_location[1] + random_number(-100.0, 100.0),
                                    ),
                                )?;
                            } else if frame % 3600 <= 400 {
                                info!(logger, "Army attacking towards enemy base");
                                unit.fight(
                                    &[UnitCommandOptions::ShiftKey],
                                    None,
                                    None,
                                    GroundPosition(
                                        nearest_enemy_location[0] + random_number(-100.0, 100.0),
                                        nearest_enemy_location[1] + random_number(-100.0, 100.0),
                                    ),
                                )?;
                            } else {
                                info!(logger, "Defending with army");
                                let target_unit = random_choice(&team_units);
                                let target_position = target_unit.position()?;
                                unit.move_unit(
                                    &[UnitCommandOptions::ShiftKey],
                                    None,
                                    Some(frame + 10000),
                                    GroundPosition(
                                        target_position[0] + random_number(-100.0, 100.0),
                                        target_position[2] + random_number(-100.0, 100.0),
                                    ),
                                )?;
                            }
                        } else {
                            let target_unit = random_choice(&team_units);
                            let target_position = target_unit.position()?;
                            info!(logger, "Defending with army");
                            unit.move_unit(
                                &[UnitCommandOptions::ShiftKey],
                                None,
                                Some(frame + 10000),
                                GroundPosition(
                                    target_position[0] + random_number(-100.0, 100.0),
                                    target_position[2] + random_number(-100.0, 100.0),
                                ),
                            )?;
                        }
                    }
                }
            }
        }
        let time = start.elapsed().as_secs_f64();
        if time > 0.05 {
            warn!(logger, "Update on frame {frame} took {time:.2} seconds");
        }
    }

    Ok(())
}
