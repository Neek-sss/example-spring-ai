use std::error::Error;

use slog::{info, Logger};
use spring_ai_rs::{
    ai_interface::{
        callback::{unit::Unit, unit_def::UnitDef},
        AIInterface,
    },
    ai_macro::ai_func,
};

#[ai_func("unit_destroyed")]
fn unit_destroyed(
    logger: &Logger,
    ai_interface: AIInterface,
    unit: Unit,
    attacker: Unit,
) -> Result<(), Box<dyn Error>> {
    let factory_defs = ai_interface
        .load_memory::<Vec<UnitDef>>("SimpleFactoryDefs")
        .unwrap();
    let extractor_defs = ai_interface
        .load_memory::<Vec<UnitDef>>("SimpleExtractorDefs")
        .unwrap();

    if factory_defs.contains(&unit.unit_def()?) {
        info!(logger, "Destroyed a factory");
        let factory_count = ai_interface
            .load_memory::<usize>("SimpleFactoryCount")
            .unwrap_or(1)
            - 1;
        ai_interface.store_memory("SimpleFactoryCount", &factory_count);
    } else if extractor_defs.contains(&unit.unit_def()?) {
        info!(logger, "Destroyed a mex");
        let mexes = ai_interface
            .load_memory::<usize>("SimpleT1Mexes")
            .unwrap_or(1)
            - 1;
        ai_interface.store_memory("SimpleT1Mexes", &mexes);
    }

    Ok(())
}
