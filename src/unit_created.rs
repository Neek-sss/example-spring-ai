use std::error::Error;

use slog::{info, Logger};
use spring_ai_rs::{
    ai_interface::{
        callback::{fire_state::FireState, move_state::MoveState, unit::Unit, unit_def::UnitDef},
        AIInterface,
    },
    ai_macro::ai_func,
};

#[ai_func("unit_created")]
fn unit_created(
    logger: &Logger,
    ai_interface: AIInterface,
    unit: Unit,
    builder: Unit,
) -> Result<(), Box<dyn Error>> {
    unit.set_fire_state(&[], None, None, FireState::FireAtWill)?;
    unit.set_move_state(&[], None, None, MoveState::Roam)?;

    let factory_defs = ai_interface
        .load_memory::<Vec<UnitDef>>("SimpleFactoryDefs")
        .unwrap();
    let extractor_defs = ai_interface
        .load_memory::<Vec<UnitDef>>("SimpleExtractorDefs")
        .unwrap();

    if factory_defs.contains(&unit.unit_def()?) {
        info!(logger, "Starting to build a factory");
        let factory_count = ai_interface
            .load_memory::<usize>("SimpleFactoryCount")
            .unwrap_or(0)
            + 1;
        ai_interface.store_memory("SimpleFactoryCount", &factory_count);
    } else if extractor_defs.contains(&unit.unit_def()?) {
        info!(logger, "Starting to build a mex");
        let mexes = ai_interface
            .load_memory::<usize>("SimpleT1Mexes")
            .unwrap_or(0)
            + 1;
        ai_interface.store_memory("SimpleT1Mexes", &mexes);
    } else {
        info!(logger, "Starting to build something");
    }

    Ok(())
}
